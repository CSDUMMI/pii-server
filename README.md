# An encrypted private server for personally identifiable information
The goal of this small project
is to serve as a PoC for centralized, trustworthy
service for private data.

# The concept
Today people are required
to provide personal information on a regular
basis to many different people.

Most prominently due to COVID-19, people have
to share their medical status with businesses,
the state and employers.

But already before this
pandemic, sharing personal
data has been a stable of the
internet.

Whether for buying in an online shop,
talking to a support team and in many
other situations.

This data is then copied, stored and
often later exposed due to bad security
practices, a malware or ransomware attack,
social engineering or because of any number
of other reasons.

Since the passing of the GDPR
EU Citizens have the right to
have their data deleted from
data holders.
But doing so is often not
possible, if they still want
to use a service.

Either because that service
can truly not be performed
without some personal data
or because the companies
providing these services
want to gather as much information
as they possibly can.

This is not a great situation
for the users, because
their data is spread across
many different data controllers. (services, companies, authorities, etc.)
Many of whom they are unaware of.

In this situation the personal data
of the users is a very vulnerable target,
since they a) don't know all the places
where their data is b) can't delete
their data, without losing access to a
service c) the data is stored by many
different actors, not all of whom
are treading the data in the best
and most secure manner.

To improve this situation,
I developed this concept.
It's goals are:
1. To gather the personal data of a user in one central location
2. That the data of the user is under the control of the user.
3. That the data is secure at rest.
4. That the user may have to trust the location as little as possible.
4. That third-party services can receive read and/or write access
to parts of the data. (If granted by the user).
5. That third-party services can write and read the data through a
standardized API.

To achieve these 6 goals, I have developed
this concept:

# Concept: Personal Data Servers
## Server
The Server manages a database
containing data from one or more
users.

The Server has a public key pair (see [Key Codes](#key-codes))

At rest the server is unable to read the data
of any of their users.
Only if data is changed or requested
and the encryption / decryption is done
on the server, does the server have access
to that data.

## User
A User has a user-supplied password, masterkey
a public key pair and entry keys.

ONLY the password is supplied by the user.
All other keys are generated using cryptographically secure
pseudorandom generators. (For which may be provided a seed by user interaction).

The password is not stored on the server.
The masterkey is encrypted using the password with AES.
The private key from the key pair and the entry keys are each encrypted
with the masterkey.

## Entries
Each entry is encrypted using a generated
key, which is only used for this one entry.

This key is then encrypted & stored using the
masterkey.

Accessing an entry thus requires this generated
key and this requires the masterkey.

Entries are indexed by way of UUIDv4 ids.
This is to prevent any association between the
entry's content and it's index.

## Key Codes
Access to entries is granted through the
use of key codes.

A key code consists of four
parts:
1. The key for the entry, encrypted using the public key of the server (as crypto-operator)
2. A payload for specifying additional restrictions for the server (crypto-operator)
on the access provided by this Access Code. (like nonces for one time uses,
write access, etc.). This is also encrypted using the public key of the
server (crypto-operator).
3. A signature of the user's private key of the payload and key ciphertext.
4. The UUID of the entry. Encrypted with the public key of the server (crypto-operator).

To process a key code a service has to send
the key code to the server (crypto-operator),
who verifies the signature of the users,
decrypts the payload to validate possible
additional conditions on the access.
If all conditions are met and the signature
is valid, the server (crypto-operator) decrypts
the key and entry.

After that it returns the entry's content to
the service.

### Crypto Operators
The crypto operator is the machine that executes
this process.
In this PoC the Crypto Operator is the server,
which is why the Key Code uses the server's public key.

Only the crypto operator can process Key Codes
and decrypt the plaintext data, since it's public
key was used for encrypting the key and payload.

If a user does not trust the server with this
process (since the crypto-operator can read the
data and enforces the restrictions), they
may setup a separate machine and authenticate
them with the server (using public key crypto)
to process any key code access requests.
Then the server should provide this machine
in a secure channel all the data required
to process the key codes forwarded to them.

## Why Key Codes?
An Alternative to Key Codes is to
just share the keys directly, without
any further signatures or encryption.

But this has several downsides, specifically:
1. It does not permit the implementation of additional
restrictions in the payload.
2. In case of the database
being compromised or leaked, the services with the key
might be able to access the data without the users knowledge or control.
(Violating goal #2)

Thus Key Codes are both a protection against the case,
that the server is compromised and it allows for additional
functionality.


## Sharing key codes
<img src="sample.png" width=250 height=250>
<img src="sample-location-key.png" width=250 height=250>

*Left: QR Code of the full key code. Right: QR Code of a location and AES Key (96 bytes).*


Instead I propose the following:
1. The key code is again encrypted using a single 16 bytes key.
2. The key code is uploaded in this encrypted form to a server.
3. The key and the location of the server with the cipher text
as well as additional information about the storage server for
the entries are put into a QR Code.

# Request for Comment
This PoC is not nearly ready for use, deployment or even an alpha phase.
I cannot put a version number on
the project on this.

The concept still requires:
1. Review of the concept for cryptographic feasibility.
2. Review of the concept for adoption feasibility.
3. Review of the code for security. An audit.
4. Review of the code for further implementation.

I ask any able to contribute to this effort
to do so. Open issues, MRs and write me with
your reviews.

CSDUMMI <[csdummi.misquality@simplelogin.co](mailto:csdummi.misquality@simplelogin.co)> 8/2021
