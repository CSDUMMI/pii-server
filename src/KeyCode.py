from Crypto.PublicKey import RSA
from Crypto.Random import get_random_bytes
from Crypto.Cipher import AES, PKCS1_OAEP
from Crypto.Signature import pkcs1_15
from Crypto.Hash import SHA256
import pickle
import qrcode
import Database as db
import uuid

PUBLIC_SERVER_KEY = RSA.import_key(open("SERVER_PUBLIC_KEY", "r").read())
PRIVATE_SERVER_KEY = RSA.import_key(open("SERVER_PRIVATE_KEY", "r").read())

class KeyCode:
    KEYS_DICT = { "session_key_ciphertext" : b""
                , "payload_ciphertext" : b""
                , "payload_tag" : b""
                , "nonce" : b""
                , "signature" : b""
                }

    @classmethod
    def __create( cls
              , session_key_ciphertext
              , payload_ciphertext
              , payload_tag
              , nonce
              , signature
              ):
        _key_code = KeyCode.KEYS_DICT.copy()
        _key_code["session_key_ciphertext"] = session_key_ciphertext
        _key_code["payload_ciphertext"] = payload_ciphertext
        _key_code["payload_tag"] = payload_tag
        _key_code["nonce"] = nonce
        _key_code["signature"] = signature
        return cls(_key_code)

    def __init__(self, _key_code):
        """Use KeyCode.create to create new objects.
        """
        self._key_code = _key_code

    @classmethod
    def create(cls, id, key, key_pair, restrictions : bytes):
        session_key = get_random_bytes(32)

        # generate session key
        cipher_rsa = PKCS1_OAEP.new(PUBLIC_SERVER_KEY)
        session_key_ciphertext = cipher_rsa.encrypt(session_key)

        # create payload
        cipher_aes = AES.new(session_key, AES.MODE_EAX)
        nonce = cipher_aes.nonce

        payload = pickle.dumps((id, key, restrictions))
        payload_ciphertext, payload_tag = cipher_aes.encrypt_and_digest(payload)

        # create signature
        frame = pickle.dumps((payload_ciphertext, payload_tag, nonce))

        hash = SHA256.new(frame)
        signature = pkcs1_15.new(key_pair).sign(hash)

        return cls.__create(session_key_ciphertext, payload_ciphertext, payload_tag, nonce, signature)

    def validate(self) -> (bytes, uuid.UUID):
        """Verify this key code to be valid.
        Return the key, entry_key_id of the key code.

        Throws exception if any decryption and verification does not work.
        """

        # Decrypt session key
        cipher_rsa = PKCS1_OAEP.new(PRIVATE_SERVER_KEY)
        session_key = cipher_rsa.decrypt(self._key_code["session_key_ciphertext"])

        # Decrypt payload
        cipher_aes = AES.new(session_key, AES.MODE_EAX, nonce=self._key_code["nonce"])
        payload = cipher_aes.decrypt_and_verify(self._key_code["payload_ciphertext"], self._key_code["payload_tag"])
        (id, key, restrictions) = pickle.loads(payload)

        entry_key = db.EntryKey.get_by_id(id)
        public_key = RSA.import_key(entry_key.user.public_key)

        # Verifys Signature
        frame = pickle.dumps((self._key_code["payload_ciphertext"], self._key_code["payload_tag"], self._key_code["nonce"]))
        hash = SHA256.new(frame)
        pkcs1_15.new(public_key).verify(hash, self._key_code["signature"])

        # Verify restrictions
        # TBD
        return (key, id)

    def __dict__(self):
        return self._key_code

    def to_list(self):
        return [self._key_code[k] for k in self._key_code]

    @classmethod
    def from_list(cls, xs):
        obj = {}

        for i, k in zip(range(len(xs)), KeyCode.KEYS_DICT):
            obj[k] = xs[i]

        cls(obj)

    def dump(self):
        return pickle.dumps(self.to_list())

    @classmethod
    def load(cls, bin_dump):
        return cls.from_list(pickle.loads(bin_dump))

    def qrcode(self):
        return qrcode.make(self.dump())
