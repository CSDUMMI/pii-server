import threading
MK_TIMEOUT = 2.0

class Masterkey:
    TIMEOUT = MK_TIMEOUT
    """Class to store masterkeys in.
    Masterkeys have to be deleted in MK_TIMEOUT
    time.
    """
    def __init__(self, masterkey : bytes, timeout = MK_TIMEOUT):
        self.masterkey = masterkey

        if timeout > MK_TIMEOUT:
            timeout = MK_TIMEOUT

        threading.Timer(timeout, self.__del__).start()

    def __del__(self):
        self.masterkey = None
        del self.masterkey
