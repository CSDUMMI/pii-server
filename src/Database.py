from Crypto.Cipher import AES, PKCS1_OAEP
from Crypto.PublicKey import RSA
from Crypto.Signature import pkcs1_15
from Crypto.Hash import SHA256
from Crypto.Random import get_random_bytes
import peewee as pw
import os
import uuid
import pickle

import KeyCode
from Masterkey import Masterkey

DATABASE_NAME = os.environ["PII_DB_NAME"]
database = pw.SqliteDatabase(DATABASE_NAME)

PUBLIC_SERVER_KEY = RSA.import_key(open("SERVER_PUBLIC_KEY", "r").read())
PRIVATE_SERVER_KEY = RSA.import_key(open("SERVER_PRIVATE_KEY", "r").read())

class BaseModel(pw.Model):
    class Meta:
        database = database

class User(BaseModel):
    id = pw.UUIDField(primary_key = True)
    masterkey = pw.BlobField()
    masterkey_nonce = pw.BlobField()
    masterkey_tag = pw.BlobField()

    public_key = pw.BlobField()

    private_key_ciphertext = pw.BlobField()
    private_key_nonce = pw.BlobField()
    private_key_tag = pw.BlobField()

    @classmethod
    def register(cls,password : bytes):
        assert len(password) == 32
        id = uuid.uuid4()
        masterkey = get_random_bytes(32)

        cipher = AES.new(password, AES.MODE_EAX)
        masterkey_nonce = cipher.nonce
        masterkey_ciphertext, masterkey_tag = cipher.encrypt_and_digest(masterkey)


        public_key_pair = RSA.generate(2048)
        private_key = public_key_pair.export_key(format = "DER")
        public_key = public_key_pair.publickey().export_key(format = "DER")

        cipher = AES.new(masterkey, AES.MODE_EAX)
        private_key_nonce = cipher.nonce
        private_key_ciphertext, private_key_tag = cipher.encrypt_and_digest(private_key)

        del masterkey
        return cls.create(
                         id = id,
                         masterkey = masterkey_ciphertext,
                         masterkey_nonce = masterkey_nonce,
                         masterkey_tag = masterkey_tag,

                         public_key = public_key,

                         private_key_ciphertext = private_key_ciphertext,
                         private_key_nonce = private_key_nonce,
                         private_key_tag = private_key_tag
                         )

    def get_masterkey(self, password : bytes, timeout : float = Masterkey.TIMEOUT) -> Masterkey:
        """Fetch the masterkey with it's password.
        Returns Masterkey class, which
        will delete the key after a timeout.

        Throws error if masterkey has been tampered with.
        """
        cipher = AES.new(password, AES.MODE_EAX, nonce=self.masterkey_nonce)
        mk = Masterkey(cipher.decrypt(self.masterkey), timeout=timeout)
        cipher.verify(self.masterkey_tag)
        return mk

    def create_entry(self, masterkey : Masterkey, data : bytes):
        """Adds a new entry encrypted
        """
        return EntryKey.create_entry(self, masterkey, data)

    def get_entry(self, entry_key, key):
        return entry_key.entry.decrypt(key)

    def get_key_pair(self, masterkey : Masterkey):
        cipher = AES.new(masterkey.masterkey, AES.MODE_EAX, nonce=self.private_key_nonce)
        private_key = cipher.decrypt_and_verify(self.private_key_ciphertext, self.private_key_tag)

        key_pair = RSA.import_key(private_key)

        return key_pair

    def generate_key_code(self, entry_key, masterkey : Masterkey, restrictions : bytes = b"") -> KeyCode:
        """Creates a key code for the entry key using the given restrictions
        """
        assert self == entry_key.user, "Entry not owned by user"

        id = entry_key.id
        key = entry_key.get_key(masterkey)
        key_pair = self.get_key_pair(masterkey)

        return KeyCode.KeyCode.create(id, key, key_pair, restrictions)

    def verify_key_code(self, key_code : KeyCode.KeyCode) -> (bytes, uuid.UUID):
        """Verify the key code to be valid.
        Returns the key in the key_code, id tuple to find the
        entry and decrypt it.

        Throws exceptions if the key_code is invalid.
        """
        return key_code.validate()

    def change_entry(self, entry_key, key : bytes, data : bytes):
        """Encrypt and store the new data
        in an old data with the old key.

        This does not change access rights to the data.

        If the key is invalid, causes ValueError.
        """
        entry = entry_key.entry
        entry.decrypt(key)

        entry.encrypt(data, key)


class Entry(BaseModel):
     id = pw.UUIDField(primary_key = True)

     data_ciphertext = pw.BlobField()
     data_nonce = pw.BlobField()
     data_tag = pw.BlobField()

     user = pw.ForeignKeyField(User, backref = "entries")

     def encrypt(self, data : bytes, key : bytes) -> bool:
        """Encrypt and load the data into
        this entry.

        If key is not valid, throws ValueError.

        Access rights will not be modified.
        """
        if self.data_ciphertext != b"":
         self.decrypt(key)

        cipher = AES.new(key, AES.MODE_EAX)
        data_nonce = cipher.nonce
        data_ciphertext, data_tag = cipher.encrypt_and_digest(data)

        self.data_ciphertext = data_ciphertext
        self.data_nonce = data_nonce
        self.data_tag = data_tag
        self.save()

     def decrypt(self, key):
        """Decrypt the data in entry.

        If key is not vaild, throws ValueError.
        """
        cipher = AES.new(key, AES.MODE_EAX, nonce=self.data_nonce)
        data = cipher.decrypt(self.data_ciphertext)
        cipher.verify(self.data_tag)
        return data

class EntryKey(BaseModel):
    """Key used only for one entry.
    """
    id = pw.UUIDField(primary_key = True)
    key_ciphertext = pw.BlobField()
    key_nonce = pw.BlobField()
    key_tag = pw.BlobField()

    user = pw.ForeignKeyField(User, backref = "keys")
    entry = pw.ForeignKeyField(Entry, backref = "entry", null = True)

    @classmethod
    def create_entry(cls, user : User, masterkey : Masterkey, data : bytes):
        key = get_random_bytes(32)

        cipher = AES.new(masterkey.masterkey, AES.MODE_EAX)
        key_nonce = cipher.nonce
        key_ciphertext, key_tag = cipher.encrypt_and_digest(key)


        entry = Entry.create( id = uuid.uuid4()
                            , data_ciphertext = b""
                            , data_nonce = b""
                            , data_tag = b""
                            , user = user
                            )

        entry.encrypt(data, key)


        key = cls.create( id = uuid.uuid4()
                        , key_ciphertext = key_ciphertext
                        , key_nonce = key_nonce
                        , key_tag = key_tag
                        , user = user
                        , entry = entry
                        )
        return key

    def get_key(self, masterkey : Masterkey) -> bytes:
        cipher = AES.new(masterkey.masterkey, AES.MODE_EAX, nonce=self.key_nonce)
        key = cipher.decrypt(self.key_ciphertext)
        cipher.verify(self.key_tag)
        return key


database.create_tables([User, Entry, EntryKey])
