from Crypto.Random import get_random_bytes
from Crypto.Hash import SHA256
import time
import qrcode
import Database as db
import io
import pickle

user = None
password = None
entry_key = None

def test_register_user():
    global user, password
    password = SHA256.new(get_random_bytes(1)).digest()
    user = db.User.register(password)

    assert db.User.get_by_id(user.id) == user

def test_get_masterkey():
    global user, password

    timeout = 0.001
    mk = user.get_masterkey(password, timeout=timeout)

    assert type(mk.masterkey) == bytes

    time.sleep(timeout)

    assert "masterkey" not in dir(mk)

def test_entry():
    global user, password, entry_key

    mk = user.get_masterkey(password)
    data = get_random_bytes(10)

    entry_key = user.create_entry(mk, data)

    assert user.get_entry(entry_key, entry_key.get_key(mk)) == data

def test_get_key_pair():
    global user, password
    mk = user.get_masterkey(password)

    key_pair = user.get_key_pair(mk)

    assert key_pair is not None
    assert key_pair.publickey() is not None

def test_key_code():
    global user, password, entry_key

    mk = user.get_masterkey(password)
    key = entry_key.get_key(mk)
    key_code = user.generate_key_code(entry_key, mk)

    assert (key, entry_key.id) == user.verify_key_code(key_code)

def test_change_entry():
    global user, password, entry_key

    mk = user.get_masterkey(password)
    new_data = get_random_bytes(10)
    key = entry_key.get_key(mk)

    user.change_entry(entry_key, key, new_data)

    assert user.get_entry(entry_key, key) == new_data

def generate_qr_code():
    global user, password, entry_key

    mk = user.get_masterkey(password)
    key = entry_key.get_key(mk)
    key_code = user.generate_key_code(entry_key, mk)

    key_code.qrcode().save("sample.png")

    bin = key_code.dump()
    location = get_random_bytes(80)
    key = get_random_bytes(16)

    qrcode.make(pickle.dumps((location, key))).save("sample-location-key.png")
